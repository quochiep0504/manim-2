import math
from os import wait
from manim import *
class TextAlignment(MovingCameraScene):
    config.background_color = '#323232'
    def construct(self):
        self.camera.frame.save_state()
        # thêm logo
        image = ImageMobject("/Users/quochiep/manimce/hiep/logo-100.png")
        self.add_foreground_mobject(image)
        image.height=1
        image.to_corner(UL,buff=0.5)
        # Nhóm Dot cửa sổ Mac
        dot1=Dot(color=RED)
        dot2=Dot(color=YELLOW_D)
        dot3=Dot(color=GREEN)
        vdot_1=VGroup(dot1,dot2,dot3).arrange(direction = RIGHT, buff = 0.125)
        # Nhóm lệnh Tikz
        t1 = Text("1\t"+r"\begin{tikzpicture}[line width=3mm]",font="Monaco", t2c={'[1:20]': PURPLE_A,'[20:]': YELLOW_C})
        t2 = Text("2\t\t"+r"""\draw[blue] (0,0) circle(2);""",font="Monaco",t2c={'[1:6]': BLUE_C,'[6:]': YELLOW_C})
        t3 = Text("3\t\t"+r"""\draw[red] (90:3)--(210:3)--(330:3)--cycle;""",font="Monaco",t2c={'[1:6]': BLUE_C,'[6:]': YELLOW_C})
        t4 = Text("4\t\t"+r"""\foreach \i in {0,120,240}""",font="Monaco",t2c={'[1:11]': BLUE_C,'[11:]': YELLOW_C})
        t5 = Text("5\t\t"+r"""\draw[blue,rotate=\i] (-30:2) arc(-30:30:2);""",font="Monaco",
        t2c={'[1:6]': BLUE_C,'[6:19]': YELLOW_C,'[19:21]': BLUE_C,'[21:]': YELLOW_C,})
        t6 = Text("6\t"+r"""\end{tikzpicture}""",font="Monaco",t2c={'[1:]': PURPLE_A})
        x = VGroup(t1, t2, t3, t4,t5,t6).arrange(direction=DOWN, aligned_edge=LEFT).scale_in_place(0.4)
        x.set(font='Monaco')
        y=VGroup(x).scale(0.75)
        #cửa sổ trái
        rec_1=BackgroundRectangle(y, color='#474747', fill_opacity=1,buff=0.1)
        rec_0=Rectangle(
            height=t1.height+t1.height/5,width=rec_1.width,fill_opacity=1,stroke_width=0,fill_color='#1f1f1f'
            )
        rec_0.next_to(rec_1,UP,buff=0)
        x.set_opacity(1)
        vdot_1.next_to(rec_0,LEFT,aligned_edge=LEFT,buff=-0.5)
        window_1=VGroup(rec_1,rec_0,vdot_1,x)
        window_1.to_edge(LEFT)
        # Hình vẽ Tikz
        circle=Circle(radius=2, stroke_width=30, color=BLUE)
        cir_temp=Circle(radius=3)
        triangle=Polygon(
            cir_temp.point_at_angle(90*DEGREES),
            cir_temp.point_at_angle(210*DEGREES),
            cir_temp.point_at_angle(330*DEGREES),
            stroke_width=30,color=RED)
        arc_1=Arc(radius=2,start_angle=-30*DEGREES,angle=60*DEGREES,color=BLUE,stroke_width=30)
        arc_1.set_stroke(background=False)
        arc_2=arc_1.copy().rotate_about_origin(120*DEGREES)
        arc_3=arc_1.copy().rotate_about_origin(240*DEGREES)
        y=VGroup(circle,triangle,arc_1,arc_2,arc_3)
        # Cửa sổ phải
        rec_3=BackgroundRectangle(y, color='#474747', fill_opacity=1,buff=0.5)
        rec_3.set(width=rec_3.width+0.25)
        rec_2=Rectangle(
            height=rec_0.height,width=rec_3.width,fill_opacity=1,stroke_width=0,fill_color='#1f1f1f'
            )
        rec_2.next_to(rec_3,UP,buff=0)
        vdot_2=vdot_1.copy().next_to(rec_2,LEFT,aligned_edge=LEFT,buff=-0.5)
        window_2=VGroup(rec_2,rec_3,vdot_2,y).to_edge(RIGHT)
        # nhóm toán màn hình
        mobjects=VGroup(window_2,window_1).set_x(0).arrange(LEFT)
        self.add(window_1[:3],window_2[:3])
        self.wait(2)
        self.add(window_1[3:])
        # 
        text0=Text('TÌM HIỂU LỆNH VẼ HÌNH TRONG TIKZ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT')
        text0.next_to(window_1,UP)
        text0.scale_to_fit_width(window_1.width)
        self.play(Write(text0))
        self.wait()
        text = VGroup(
            Text('Đây là code hình vẽ của nhóm ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            Text('Vẽ hình khoa học TikZ - Asymptote',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT')
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        text.scale_to_fit_width(window_1.width)
        height_text=text[0].height
        self.play(Write(text))
        # 
        self.wait(2)
        self.add(window_2[3:])
        self.wait(2)
        rec_4=SurroundingRectangle(t1[21:34], GREEN)
        self.play(Create(rec_4))
        self.wait()
        # 
        self.play(FadeOut(text,shift=DOWN))
        self.wait()
        text = VGroup(
            Text('Độ dày của nét vẽ ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        # 
        self.play(self.camera.frame.animate.scale(0.5))
        p1,p2=-2.1*LEFT,-1.9*RIGHT
        d1 = DoubleArrow(p1, p2, buff=0, tip_length=0.1, color=YELLOW).scale(0.08).move_to(circle.point_at_angle(170*DEGREES))
        d2=d1.copy().rotate_about_origin(-30*DEGREES
        ).move_to(0.5*(triangle.point_from_proportion(0)+triangle.point_from_proportion(1/3)))
        t1_width=t1.copy()[31:34].next_to(d1,UP,buff=0.1)
        t2_width=t1.copy()[31:34].next_to(d2,UP,buff=0.1).rotate(-30*DEGREES,about_point=d2.get_center())
        self.play(FadeIn(d1))
        self.play(TransformFromCopy(t1[31:34],t1_width))
        self.wait()
        self.play(FadeIn(d2))
        self.play(TransformFromCopy(t1[31:34],t2_width))
        self.wait()
        self.play(FadeOut(VGroup(d1,t1_width,d2,t2_width)))
        self.wait()
        self.play(Restore(self.camera.frame))
        self.wait()
        self.play(FadeOut(rec_4))
        self.wait()
        x[1:-1].set_opacity(0.3)
        self.remove(y)
        x[1].set_opacity(1)
        self.play(FadeOut(text,shift=DOWN))
        self.wait()
        text = VGroup(
            Text('Lệnh vẽ đường tròn',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        # truc Oxy
        ox = Arrow(3*LEFT+circle.get_center(),3*RIGHT+circle.get_center(), buff=0, tip_length=0.2, color=YELLOW,stroke_width=3)
        oy = Arrow(2.5*DOWN+circle.get_center(),3.5*UP+circle.get_center(), buff=0, tip_length=0.2, color=YELLOW,stroke_width=3)
        ox_lable,oy_lable=Tex('$x$').next_to(ox.get_end(),UL),Tex('$y$').next_to(oy.get_end(),DL)
        oxy=VGroup(ox,oy,Dot(circle.get_center()),ox_lable.scale_in_place(0.75),oy_lable.scale_in_place(0.75))
        #
        self.wait()
        self.play(Create(circle))
        self.wait()
        p3,p4=circle.point_at_angle(170*DEGREES),1.5*circle.point_at_angle(150*DEGREES)
        p1,p2=circle.get_center(),circle.point_at_angle(180*DEGREES)
        d1 = DoubleArrow(p1, p2, buff=0, tip_length=0.2, color=RED).scale(1)
        d2 = Arrow(p4, p3, buff=0, tip_length=0.2, color=YELLOW).scale(1)
        d1_text1=t2.copy()[24:25].next_to(d1,UP,buff=0.1).scale_in_place(1.5)#2
        d1_text2=t2.copy()[12:17].next_to(d1.get_start(),DR,buff=0.3).scale_in_place(1.5)#(0,0)
        d2_text=t2.copy()[7:11].next_to(p4,UR,buff=0.1).set_color(BLUE).scale_in_place(1.5)#blue
        print(len(t2))
        rec_4=SurroundingRectangle(t2[7:11], GREEN)
        rec_5=SurroundingRectangle(t2[12:17], GREEN)
        rec_6=SurroundingRectangle(t2[24:25], GREEN)
        self.play(Create(rec_4))
        # 
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Màu sắc nét vẽ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        # text.scale_to_fit_height(height_text)
        self.play(Write(text))
        self.wait()
        # 
        self.play(Indicate(d2))
        self.play(TransformFromCopy(t2.copy()[7:11],d2_text))
        self.wait()
        self.play(ReplacementTransform(rec_4,rec_5))
        # 
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Tọa độ tâm đường tròn',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(Create(oxy))
        self.wait()
        self.play(TransformFromCopy(t2.copy()[12:17],d1_text2))
        self.wait()
        self.play(ReplacementTransform(rec_5,rec_6))
        self.wait()
        # 
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Bán kính đường tròn',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        print(text.height)
        self.play(Write(text))
        self.wait()
        # 
        self.play(Indicate(d1))
        self.wait()
        self.play(TransformFromCopy(t2.copy()[24:25],d1_text1))
        self.wait()
        self.play(FadeOut(rec_6))
        self.wait()
        self.play(FadeOut(VGroup(d1,d2,d1_text1,d1_text2,d2_text,oxy)))
        self.wait()
        # line2
        x[2].set_opacity(1)#59
        # 
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Lệnh vẽ đoạn thẳng',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        print(text.height)
        self.play(Write(text))
        # 
        self.wait()
        self.play(Create(triangle))
        self.wait()
        # print(len(t3))
        rec_4=SurroundingRectangle(t3[7:10], GREEN)
        rec_5=SurroundingRectangle(t3[15:16], GREEN)
        rec_6=SurroundingRectangle(t3[11:17], GREEN)
        rec_7=SurroundingRectangle(t3[19:26], GREEN)
        rec_8=SurroundingRectangle(t3[28:35], GREEN)
        p1,p2=0.5*(triangle.point_from_proportion(0)+triangle.point_from_proportion(1/3)),triangle.get_center()
        d1 = Arrow(p1, p2, buff=0, tip_length=0.2, color=YELLOW).scale(1)
        d1_text=t3.copy()[7:10].next_to(p2,RIGHT,buff=0.1).set_color(RED).scale_in_place(1.5)
        v1_text=t3.copy()[15:16].next_to(circle.get_center()+1.5*RIGHT,UP,buff=0.2).scale_in_place(1.5)
        v2_text=t3.copy()[11:17].next_to(triangle.point_from_proportion(0),UR,buff=0.3).scale_in_place(1.5)
        v3_text=t3.copy()[19:26].next_to(triangle.point_from_proportion(1/3),DOWN,buff=0.2).scale_in_place(1.5)
        v4_text=t3.copy()[28:35].next_to(triangle.point_from_proportion(2/3),DOWN,buff=0.2).scale_in_place(1.5)
        self.wait()
        self.play(Create(rec_4))
        # 
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Màu sắc nét vẽ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        # text.scale_to_fit_height(height_text)
        self.play(Write(text))
        # 
        self.wait()
        self.play(Indicate(d1))
        self.wait()
        self.play(TransformFromCopy(t3.copy()[7:10],d1_text))
        self.wait()
        self.play(FadeOut(VGroup(d1,d1_text)))
        
        self.wait()
        # 
        theta_tracker = ValueTracker(0.1)
        line1 = Line(circle.get_center(),3*RIGHT+circle.get_center())
        line_moving = Line(circle.get_center(),3*RIGHT+circle.get_center())
        dot4=Dot(line_moving.get_end())
        line_ref = line_moving.copy()
        # self.add(line1,line_moving)
        line_moving.rotate(
            theta_tracker.get_value() * DEGREES, about_point=circle.get_center()
        )
        a = Angle(line1, line_moving, radius=0.5, other_angle=False)
        
        self.play(ReplacementTransform(rec_4,rec_5))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Tọa độ cực của điểm thứ 1',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        self.wait()
        self.add(oxy)
        self.wait()
        self.add(line1, line_moving,a,dot4)
        # 
        self.wait()
        self.play(TransformFromCopy(t3.copy()[15:16],v1_text))
        self.wait()
        def dot_position(mobject):
            mobject.set_value(theta_tracker.get_value())
            mobject.move_to(
            Angle(
                line1, line_moving, radius=0.5 + 4 * SMALL_BUFF, other_angle=False
            ).point_from_proportion(0.5)
        )
        label = DecimalNumber(num_decimal_places=0)
        t1=Tex('$^\circ$')
        label.add_updater(dot_position)
        t1.add_updater(lambda x: x.next_to(label,UR,buff=0))
        vg=VGroup(label,t1)
        vg.add_updater(lambda x: x.move_to(
            Angle(
                line1, line_moving, radius=0.5 + 4 * SMALL_BUFF, other_angle=False
            ).point_from_proportion(0.5)))
        self.add(vg)
        # self.add(label)
        line_moving.add_updater(
            lambda x: x.become(line_ref.copy()).rotate(
                theta_tracker.get_value() * DEGREES, about_point=circle.get_center()
            )
        )

        a.add_updater(
            lambda x: x.become(Angle(line1, line_moving, radius=0.5, other_angle=False))
        )
        dot4.add_updater(
            lambda x:x.move_to(circle.get_center()+[3*math.cos(theta_tracker.get_value()*DEGREES),3*math.sin(theta_tracker.get_value()*DEGREES),0])
        )
        self.play(ReplacementTransform(rec_5,rec_6))
        #
        self.wait()
        self.play(theta_tracker.animate.set_value(90))
        self.wait()
        # 
        dot5=Dot(line_moving.get_end())
        self.add(dot5)
        # 
        self.play(TransformFromCopy(t3.copy()[11:17],v2_text))
        self.wait()
        self.play(ReplacementTransform(rec_6,rec_7))
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Tọa độ cực của điểm thứ 2',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        self.wait()
        self.play(theta_tracker.animate.set_value(210))
        self.wait()
        self.play(TransformFromCopy(t3.copy()[19:26],v3_text))
        self.wait()
        self.play(ReplacementTransform(rec_7,rec_8))
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Tọa độ cực của điểm thứ 3',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        self.wait()
        # 
        dot6=Dot(line_moving.get_end())
        self.add(dot6)
      
        # 
        self.play(theta_tracker.animate.set_value(330))
        self.wait()
        self.play(TransformFromCopy(t3.copy()[28:35],v4_text))
        self.wait()
        self.play(FadeOut(VGroup(rec_8,oxy,vg,line_moving,line1,a,dot4,dot5,dot6,
        v1_text,v2_text,v3_text,v4_text)))
        # line4
        self.wait()
        x[3:5].set_opacity(1)
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Lệnh vẽ trong vòng lặp',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        rec_4=SurroundingRectangle(t4[9:11], GREEN)#'\i'
        rec_5=SurroundingRectangle(t4[14:15], GREEN)#'0'
        rec_6a=SurroundingRectangle(t4[16:19], GREEN)#'120'
        rec_6b=SurroundingRectangle(t4[20:23], GREEN)#'240'
        rec_7=SurroundingRectangle(t5[20:21], GREEN)
        rec_8=SurroundingRectangle(t5[22:29], GREEN)
        rec_9=SurroundingRectangle(t5[29:-1], GREEN)
        rec_10_a=SurroundingRectangle(t5[22:-1],GREEN)
        rec_10_b=rec_10_a.copy()
        dot4=Dot(arc_1.get_start())
        self.play(Create(rec_4))
        rec_7_1=rec_7.copy()
        rec_7_2=rec_7.copy()
        rec_7_3=rec_7.copy()
        self.play(Create(rec_7_1))
        self.wait()
        self.play(ReplacementTransform(rec_4,rec_5))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Giá trị biến bằng 0',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        # text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        self.wait()
        # self.play(TransformFromCopy(t5[22:29],v2_text))
        # self.wait()
        v1_text=t4.copy()[14:15].next_to(rec_7,DOWN,buff=0.1).scale_in_place(1.5)
        v2_text=t5.copy()[22:29].next_to(dot4,DOWN,buff=0.1).scale_in_place(1.5)
        num_0=t4[14:15].copy()
        self.play(num_0.animate.move_to(t5[20:21]))
        self.play(Transform(num_0,v1_text))
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Quay hình vẽ thu được ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            Tex('$0^\circ$').set_color(GREEN).scale_to_fit_height(0.53).shift(0.1*UP),
            ).scale(0.85).arrange(direction = RIGHT,aligned_edge=UP,buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.add(oxy)
        self.wait()
        self.play(ReplacementTransform(rec_7_1,rec_8))
        #
        self.play(FadeOut(text,shift=DOWN))
        print('bắt đầu')#130
        text = VGroup(
            Text('Tọa độ cực đầu bút vẽ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(TransformFromCopy(t5[22:29],v2_text))
        
        self.wait()
        self.add(dot4)
        self.play(ReplacementTransform(rec_8,rec_9))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Một cung tròn có bán kính 2',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            Text('số đo cung bằng 60 độ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        self.wait()
        v10_text=t4.copy()[16:19].next_to(rec_7,DOWN,buff=0.1).scale_in_place(1.5)
        v11_text=t4.copy()[20:23].next_to(rec_7,DOWN,buff=0.1).scale_in_place(1.5)
        self.wait()
        arc_1_temp=arc_1.copy().set_color(BLUE_E)
        self.play(Create(arc_1_temp))
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('"Đổi màu nét vẽ cho dễ nhìn!"',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(FadeOut(VGroup(dot4,v2_text,num_0)))
        self.wait()
        self.play(ReplacementTransform(rec_9,rec_7_2))
        self.wait()
        self.play(ReplacementTransform(rec_5,rec_6a))
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Giá trị biến bằng 120',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        num_120=t4[16:19].copy()
        self.play(num_120.animate.move_to(t5[20:21]))
        self.play(Transform(num_120,v10_text))
        self.wait()
        self.play(ReplacementTransform(rec_7_2,rec_10_a))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Quay hình vẽ thu được ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            Tex('$120^\circ$').set_color(GREEN).scale_to_fit_height(0.53).shift(0.1*UP),
            ).scale(0.85).arrange(direction = RIGHT,aligned_edge=UP,buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(Rotating(arc_1_temp.copy(),radians=120*DEGREES,about_point=circle.get_center()),run_time=2)
        self.wait()
        self.play(FadeOut(num_120))
        self.wait()
        self.play(ReplacementTransform(rec_10_a,rec_7_3))
        self.play(ReplacementTransform(rec_6a,rec_6b))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Giá trị biến bằng 240',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        # text.scale_to_fit_width(window_1.width)
        self.play(Write(text))
        num_240=t4[20:23].copy()
        self.play(num_240.animate.move_to(t5[20:21]))
        self.play(Transform(num_240,v11_text))
        self.play(ReplacementTransform(rec_7_3,rec_10_b))
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('Quay hình vẽ thu được ',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            Tex('$240^\circ$').set_color(GREEN).scale_to_fit_height(0.53).shift(0.1*UP),
            ).scale(0.85).arrange(direction = RIGHT,aligned_edge=UP,buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(Rotating(arc_1_temp.copy(),radians=240*DEGREES,about_point=circle.get_center()),run_time=2)
        self.wait()
        self.play(FadeOut(VGroup(oxy,num_240,rec_10_b,rec_6b)))
        self.wait()
        #
        self.play(FadeOut(text,shift=DOWN))
        text = VGroup(
            Text('"Trả lại màu nét vẽ!"',gradient=(BLUE, GREEN),font='Helvetica Neue',weight='ULTRALIGHT'),
            ).scale(0.85).arrange(direction = DOWN, buff = 0.125)
        text.next_to(window_1,DOWN,buff=1)
        self.play(Write(text))
        self.wait()
        self.play(VGroup(arc_1,arc_2,arc_3).animate.set_color(BLUE))
        self.wait(2)
        picture= Group(*self.mobjects)
        picture.remove(image)
        self.play(FadeOut(picture),run_time=2)
