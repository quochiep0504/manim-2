from manim import *
class Func:
    def text_scene(text):
            text1=Tex(text).set_color_by_gradient(RED_D,BLUE_D)
            # text1.scale(0.85)
            text1.to_edge(DOWN,buff=1)
            rec=BackgroundRectangle(text1, color=DARK_GREY, fill_opacity=0.75,buff=0.1)
            anim=[]
            anim.append(FadeIn(rec,shift=2*RIGHT))
            anim.append(FadeIn(text1,shift=2*LEFT,run_time=2))
            anim.append(Uncreate(rec,run_time=1))
            anim.append(Uncreate(text1.reverse_direction(),run_time=1))
            return anim
    def title_graph(self,i):
            title = Tex("Xác định đồ thị của hàm số", color=WHITE)
            title.scale_in_place(1)
            title.to_edge(UP)
            t1 = Tex("1. $y=f(x)+a$ ($a>0$)").set_color(WHITE)
            t2 = Tex("2. $y=f(x)-a$ ($a>0$)").set_color(WHITE)
            t3 = Tex("3. $y=f(x-a)$ ($a>0$)").set_color(WHITE)
            t4 = Tex("4. $y=f(x+a)$ ($a>0$)").set_color(WHITE)
            t5 = Tex("5. $y=-f(x)$ ").set_color(WHITE)
            t6 = Tex("6. $y=f(-x)$ ").set_color(WHITE)
            t7 = Tex("7. $y=\\big|f(x)\\big|$ ").set_color(WHITE)
            t8 = Tex("8. $y=f\\big(|x|\\big)$ ").set_color(WHITE)
            x = VGroup(t1, t2, t3, t4,t5,t6,t7,t8).arrange(direction=DOWN, aligned_edge=LEFT).scale_in_place(1)
            x.next_to(title,DOWN)
            y=VGroup(title,x).scale(0.75)
            y.to_edge(LEFT)
            rec=BackgroundRectangle(y, color=RED, fill_opacity=1,buff=0.1).set_sheen(-0.3, DR)
            x.set_opacity(0.3)
            y.add(rec,x,title)
            if i is 1:
                self.add(y)
                self.wait()
                self.play(x.submobjects[0].animate.set_opacity(1))
                self.wait()
            else:
                x.submobjects[i-2].set_opacity(1)
                self.add(y)
                self.wait()
                self.play(AnimationGroup(
                    x.submobjects[i-2].animate.set_opacity(0.3),
                    x.submobjects[i-1].animate.set_opacity(1),
                    ))
                self.wait()
            self.play(FadeOut(y)
                    )
class Move0(Scene):
    def construct(self):
        image = ImageMobject("/Users/quochiep/manimce/hiep/logo-100.png")
        self.add_foreground_mobject(image)
        # image.set_opacity(0.5)
        image.height=1
        image.to_corner(UR,buff=0.5)
        ax=NumberPlane(
            background_line_style={
                        # "stroke_color": TEAL,
                        "stroke_width": 2,
                        "stroke_opacity": 0.6
                    },
            # axis_config = {
            #             "include_tip": True,
        # }
        ).scale(1)
        def func(x):
            return 0.5*(x**2-1)*(x-2)
        graph_1 = ax.get_graph(func, color=BLUE)
        line_Ox=Arrow(config.frame_width*RIGHT/2*LEFT,config.frame_width*RIGHT/2,buff=0,stroke_width=1,tip_length=0.25).set_color(WHITE)
        lable_Ox=Tex('$x$').next_to(line_Ox.get_end(),DL)
        line_Oy=Arrow(config.frame_height*DOWN/2*LEFT,config.frame_height*UP/2,buff=0,stroke_width=1,tip_length=0.25).set_color(WHITE)
        lable_Oy=Tex('$y$').next_to(line_Oy.get_end(),DL)
        sys_coord=VGroup(ax,line_Ox,line_Oy,lable_Ox,lable_Oy)
        self.play(Create(sys_coord))
        # block1
        a=Func.text_scene('Cho đồ thị hàm số $y=f(x)$')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.play(Create(graph_1,rate_func=rate_functions.ease_in_out_sine),run_time=2)
        graph1_label = ax.get_graph_label(
            graph_1, "y=f(x)", x_val=2.75, direction=UR, color=BLUE
        )
        self.play(Create(graph1_label))
        dot1,dot2,dot3,dot4=Dot([-1,0,0]),Dot([1,0,0]),Dot([2,0,0]),Dot([0,1,0])
        group_dot=VGroup(dot1,dot2,dot3,dot4)
        self.play(AnimationGroup(
            FadeIn(group_dot[0],scale=2),
            FadeIn(group_dot[1],scale=2),
            FadeIn(group_dot[2],scale=2),
            FadeIn(group_dot[3],scale=2),
            lag_ratio=0.3
        ))
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        Func.title_graph(self,1)
        a=Func.text_scene('Đồ thị hàm số $y=f(x)+a~(a>0)$ được xác định như sau')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait(2)
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        a=Func.text_scene('Tịnh tiến đồ thị hàm số $y=f(x)$ lên trên $a$ đơn vị')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait()
        dash1=DashedVMobject(graph_1,num_dashes=int(graph_1.get_arc_length()/3))
        self.add(dash1)
        self.play(graph_1.animate.set_color(GOLD_E))
        self.play(TransformFromCopy(graph_1.copy(),graph_1.shift(UP)),run_time=2)
        graph_label = ax.get_graph_label(
            graph_1, "y=f(x)+a", x_val=-1.75, direction=UL, color=GOLD_E
        )
        self.play(Create(graph_label))
        line1=Arrow([0,1,0],[0,2,0],buff=0)
        line1.add_tip(at_start=True,tip_length=0.25)
        b1 = Brace(line1,direction=line1.copy().rotate(-PI/2).get_unit_vector(),buff=0.1)
        b1text = b1.get_text("a")
        group_1=VGroup(line1,b1,b1text).set_color(MAROON_B)
        self.play(
            AnimationGroup(Create(group_1),
            lag_ratio=0.3
            )
        )
        self.wait(2)
        self.play(FadeOut(group_1))
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        self.wait(2)
        picture= Group(*self.mobjects)
        picture.remove(image)
        self.play(FadeOut(picture),run_time=1)
        
class Move7(Scene):
    def construct(self):
        ax=NumberPlane(
            background_line_style={
                        # "stroke_color": TEAL,
                        "stroke_width": 2,
                        "stroke_opacity": 0.6
                    },
            # axis_config = {
            #             "include_tip": True,
        # }
        ).scale(1)
        image = ImageMobject("/Users/quochiep/manimce/hiep/logo-100.png")
        self.add_foreground_mobject(image)
        # image.set_opacity(0.5)
        image.height=1
        image.to_corner(UR,buff=0.5)
        def func(x):
            return 0.5*(x**2-1)*(x-2)
        graph_1 = ax.get_graph(func, color=BLUE)
        line_Ox=Arrow(config.frame_width*RIGHT/2*LEFT,config.frame_width*RIGHT/2,buff=0,stroke_width=1,tip_length=0.25).set_color(WHITE)
        lable_Ox=Tex('$x$').next_to(line_Ox.get_end(),DL)
        line_Oy=Arrow(config.frame_height*DOWN/2*LEFT,config.frame_height*UP/2,buff=0,stroke_width=1,tip_length=0.25).set_color(WHITE)
        lable_Oy=Tex('$y$').next_to(line_Oy.get_end(),DL)
        sys_coord=VGroup(ax,line_Ox,line_Oy,lable_Ox,lable_Oy)
        graph1_label = ax.get_graph_label(
            graph_1, "y=f(x)", x_val=2.75, direction=UR, color=BLUE
        )
        dot1,dot2,dot3,dot4=Dot([-1,0,0]),Dot([1,0,0]),Dot([2,0,0]),Dot([0,1,0])
        group_dot=VGroup(dot1,dot2,dot3,dot4)
        self.add(sys_coord,graph_1,graph1_label,group_dot)
        Func.title_graph(self,8)
        dash1=DashedVMobject(graph_1,num_dashes=int(graph_1.get_arc_length()/3))
        self.add(dash1)
        a=Func.text_scene('Đồ thị hàm số $y=f\\big(|x|\\big)$ được xác định như sau')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait(2)
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        self.wait()
        a=Func.text_scene('Giữ nguyên phần đồ thị nằm bên phải trục $Oy$')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait()
        graph_6=ax.get_graph(func, x_range=[0,graph_1.get_end()[0]], color=GOLD_E)
        self.play(ShowPassingFlash(
                graph_6.copy().set_color(WHITE),
                run_time=2,
                time_width=1
            ))
        self.play(Create(graph_6),run_time=2)
        self.wait()
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        a=Func.text_scene('Lấy đối xứng qua trục $Oy$ phần đồ thị nằm bên phải trục $Oy$')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait()
        graph_7=ax.get_graph(func, x_range=[0,graph_1.get_end()[0]], color=GOLD_E)
        graph_7.flip(axis=UP,about_point=ORIGIN)
        self.play(ShowPassingFlash(
                graph_7.copy().set_color(WHITE),
                run_time=2,
                time_width=1
            ))
        self.play(Create(graph_7),run_time=2)
        self.wait()
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        self.wait()
        a=Func.text_scene('Bỏ phần đồ thị $y=f(x)$ nằm bên trái trục $Oy$')
        self.play(
            AnimationGroup(*a[:2],
            lag_ratio=0.3
            )
        )
        self.wait()
        # graph_1.animate.set_color(GOLD_E)
        self.play(FadeOut(graph_1))
        graph_label = ax.get_graph_label(
            graph_1, "y=f\\big(|x|\\big)", x_val=-2, direction=UL, color=GOLD_E
        ).next_to(graph1_label,LEFT).shift(5.75*LEFT)
        self.play(Create(graph_label))
        self.wait()
        self.play(
            AnimationGroup(*a[2:],
            lag_ratio=0
            )
        )
        self.wait(2)
        picture= Group(*self.mobjects)
        picture.remove(image)
        self.play(FadeOut(picture),run_time=2)

